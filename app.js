const express = require('express');
const path = require('path');
const bodyParser = require('body-parser'); //parse income body
const cors = require('cors'); // allow to make api calls from  different domainname.
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const expressValidator = require('express-validator');

//set database
mongoose.connect(config.database);
//on connect
mongoose.connection.on('connected', () => {
  console.log('conn to db : ' + config.database);
});
// on error
mongoose.connection.on('error', (err) => {
  console.log('db error: ' + err);
});

const app = express();
const promos = require('./routes/promos');
// const port = 3000;
const port = process.env.PORT || 8080; // heroku deployment port

// cors middleware
app.use(cors());
// body parse middle ware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
// validatory middleare
app.use(expressValidator());

app.use('/promos', promos);
//index route
app.get('/', (req, res)=> {
  res.send('Invalid Endpoint');
});


//start server
app.listen (port, ()=> {
  console.log('Server start on port ' + port);
});

module.exports = app; // for testing