// to seed do node seeds.js
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
// const url = 'mongodb://nate:Mlab7!@ds227119.mlab.com:27119/locpromo';  // mlab mongoDB
let data;

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  let dbo = db.db('locpromo');
  //seed
  seeds(dbo);
  //list data
  // list(dbo);

  db.close();
});

function list(dbo) {
  dbo.collection('promos').find().toArray(function(err, res) {
    if (err) throw err;
    console.log(res);

    // res.forEach(element => {
    //   console.log(element.city);
    // });

  });
};

function seeds(dbo) {

  // data.forEach((value)=>{
  //   dbo.collection("customers").insertOne(value, function(err, res) {
  //   if (err) throw err;
  //   console.log("1 document inserted");
  //   console.log(value);
  // });
  
  dbo.collection('promos').insertMany(data, function(err, res) {
    if (err) throw err;
    console.log(data);
  });

  dbo.collection('promos').createIndex( { location: "2dsphere" } );
}

let latitude = 37.390309;
let longitude =  -122.08350000000002;

data = [
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "$12.50 for $25 Worth of Pizza",
    description: "More than a dozen gourmet pies, with toppings such as shrimp, sun-dried tomatoes, and feta, complement an array of salads and sandwiches",
    price: "12.50",
    category: "Food & Drink",
    street: "123 A St",
    city: "Sacramento",
    zipcode: "91234",
    location: { type: "Point", coordinates: [ -122.08350000000002, 37.390309 ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 3,
    business_id: 3,
    website: "website.url",
    image: "some.url",
    title: "One or Two Personalized Teen or Kids Pillow Cases from Monogram Online (Up to 83% Off)",
    description: "Personalized pillow cases perfect for a kid or a teen",
    category: "Shopping",
    price: 5,
    street: "234 B St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "$197 for $437 Worth of Wrestling — Pro Wrestling Revolution Training Academy",
    description: "From the merchant: One month of pro wrestling training; instructors with over 50 years of industry experience",
    category: "Health & Fitness",
    price: 197,
    street: "345 C St",
    city: "Sacramento",
    zipcode: "91234",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "One, Three, or Six Silk Peel Microdermabrasion Facials with LED Treatments at Strada Salon & Day Spa (Up to 64% Off)",
    category: "Beauty & Spas",
    description: "Intensive exfoliation aims to obscure signs of aging, and LEDs kick-start collagen production to keep skin looking youthful",
    price: 108,
    street: "456 A St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "Rockin' Jump: One Hour of Trampoline Time for Two, Four, or Six at Rockin' Jump (Up to 57% Off)",
    category: 'Things To Do',
    description: "Ten thousand square feet of trampolines for kids and adults to bounce, play dodge ball or basketball, and dive into a foam pit",
    price: 16,
    street: "678 B St",
    city: "Sacramento",
    zipcode: "94321",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 3,
    business_id: 3,
    website: "website.url",
    image: "some.url",
    title: "Bowl- or Bead-Making Class for One, or Glass-Making Experience for 2 at Bay Area Glass Institute (Up to 51% Off)",
    description: "Glass-making classes teach students how to form bowls, shape decorative beads, or cut glass to make plates and coasters",
    price: 89,
    category: 'Things To Do',
    street: "123 C St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "$29 for Emissions Testing at Green Star Smog Check ($89 Value)",
    category: "Automotive",
    description: "Vehicles get cleared for the road during a 15-minute visit to a STAR-certified smog check station that features free Wi-Fi",
    price: 29,
    street: "312 A St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "One or Three Shooting Machine or Small-Group Training Sessions at Mike Allen Sports (Up to 40% Off)",
    description: "Players’ shooting is evaluated by tech that provides feedback on speed and form; small-group sessions boost performance and clock management",
    category: 'Health & Fitness',
    price: "52.50",
    street: "543 B St",
    city: "Sacramento",
    zipcode: "94321",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 3,
    business_id: 3,
    website: "website.url",
    image: "some.url",
    title: "$12.50 for $25 Worth of Pizza, Sandwiches, and Salads at New York Pizza",
    description: "More than a dozen gourmet pies, with toppings such as shrimp, sun-dried tomatoes, and feta, complement an array of salads and sandwiches",
    price: 12.50,
    category: "Food & Drink",
    street: "654 C St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "$5 for Personalized Braided Bracelet from MonogramHub ($49.99 Value)",
    description: "The black, braided bracelet with a knot close comes with a metal charm, which can be engraved with a special message, date, or name",
    price: 5,
    category: "Shopping",
    street: "987 A St",
    city: "Sacramento",
    zipcode: "91234",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "One Haircut, Style, and Brazilian Split-End Treatment or Brazilian Blowout at Jasmine Salon (Up to 60% Off)",
    description: "Stylists give clients individual attention for high-quality haircare at salon owned by master colorist of more than two decades",
    price: 55,
    category: "Beauty & Spas",
    street: "764 B St",
    city: "Sacramento",
    zipcode: "91234",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "5 or 10 Group Dance Lessons for One or Two at Crystal Ballroom Dance Studio (Up to 70% Off)",
    description: "45-minute group lessons cover styles such as waltz, fox trot, swing, and salsa",
    price: 25,
    category: "Things To Do",
    street: "890 C St",
    city: "Sacramento",
    zipcode: "91234",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 3,
    business_id: 3,
    website: "website.url",
    image: "some.url",
    title: "Dental Package with Option for Take-Home Whitening at Jamah Dental (Up to 92% Off)",
    description: "Comfort-focused dental team check up on teeth with complete checkup, optional whitening brightens smiles",
    price: 39,
    category: "Heath & Fitness",
    street: "987 A St",
    city: "Sacramento",
    zipcode: "94321",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "Lizette Calderon at Getting To The Root: One or Two 60-Minute Massages and Body Scrubs from Lizette Calderon at Getting To The Root (Up to 72% Off)",
    description: "Massage therapist reduces stress, harmonizes emotions, and increases circulation with a therapeutic massage, body scrub, and essential oils",
    price: 59,
    category: "Beauty & Spas",
    street: "520 B St",
    city: "Sacramento",
    zipcode: "94321",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "$69 for General Carpet Cleaning for Up to 600 Sq. Ft. from Clean Green Carpets ($282 Value)",
    category: "Home Services",
    description: "Eco-friendly cleaning process and cleaning solution safe for families and pets",
    price: 69,
    street: "614 C St",
    city: "Sacramento",
    zipcode: "91234",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 3,
    business_id: 3,
    website: "website.url",
    image: "some.url",
    title: "Bacon and Beer Classic on Saturday, November 14",
    description: "Unlimited bacon and bacon-inspired dishes served with craft beers at Municipal Stadium",
    price: 49,
    category: "Food & Drink",
    street: "457 A St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "One or Three Holistic Stress Release-Alignment Treatments at Alphabiotics Center Bay Area (Up to 54% Off)",
    description: "Alphabiotic alignment involves a quick movement of the head, resulting in a release of previously misdirected energy to balance the body",
    price: 19,
    category: "Health & Fitness",
    street: "126 B St",
    city: "Sacramento",
    zipcode: "94321",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 3,
    business_id: 3,
    website: "website.url",
    image: "some.url",
    title: "2, 4, or 6 Groupons Good for Paint-Your-Own-Pottery at Color Me Mine in San Jose and Oakridge Mall (44% Off)",
    description: "Choose from a bounty of dishes, figurines, and other ceramic creations to paint in vibrant colors, then hand them to staff to glaze and fire",
    price: 5,
    category: "Shopping",
    street: "278 C St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 1,
    business_id: 1,
    website: "website.url",
    image: "some.url",
    title: "Photo Package for Up to Two or Four with Prints and Digital Photos at Perfect Studio (Up to 51%Off)",
    description: "Photo studio that has been in business for more than 15 years captures images of groups of up to two or four",
    price: 27,
    category: "Local Services",
    street: "394 A St",
    city: "Sacramento",
    zipcode: "94321",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  },
  {
    user_id: 2,
    business_id: 2,
    website: "website.url",
    image: "some.url",
    title: "$99 for a Dental Exam, X-rays, and Cleaning at San Jose Smile Center ($350 Value)",
    description: "Exam and digital x-rays determine if the patient has any cavities before a cleaning removes plaque and tartar",
    price: "99.99",
    category: 'Local Services',
    street: "274 B St",
    city: "Sacramento",
    zipcode: "90010",
    location: { type: "Point", coordinates: [ longitude, latitude ] },
    temp: "something extra",
    expires_at: "2016-01-01 23:59:59"
  }
];