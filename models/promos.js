const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

 //user schema
const PromoSchema = mongoose.Schema ({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: {type: String, require: true},
  business_id: {type: String},
  website: {type: String},
  image: {type: String},
  title: {type: String},
  category: {type: String},
  description: {type: String},
  price: {type: Number},
  street: {type: String},
  city: {type: String},
  zipcode: {type: Number},
  location: { type: {type:String}, coordinates: [ {type:Number}, { type:Number }] },
  temp: {type: String},
  expires_at: {type: String}
});

const Promos = module.exports = mongoose.model('promos', PromoSchema);
 
module.exports.getList = (long, lat, distance, offset, limit=10, callback)=> {
  Promos.find({location:
                  { $near:
                    { $geometry: { type: 'Point',  coordinates: [ long, lat ] },
                      $maxDistance: distance
                    }
                  }
              }).
          skip(parseInt(offset)).
          limit(parseInt(limit)).
          sort({'expires_at' :-1}).
          exec(callback);

  // Promos.
  // find({}).
  // where('price').gt(12).
  // skip(3).
  // // limit(3).
  // sort({'price':-1}).
  // // select('_id  price city').
  // exec(callback);
};

module.exports.doDelete =(id, callback) => {
  // console.log(id);

  Promos.remove({'_id':id}).
               exec(callback);

  // Promos.remove({ expires_at: '2018-01-01 23:59:59'}).
  //              exec(callback);
               
}; 
 