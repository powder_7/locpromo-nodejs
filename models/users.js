const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//user schema
const UserSchema = mongoose.Schema ({
  name : {
    type: String
  },
  email : {
    type : String,
    require: true
  },
  username : {
    type: String,
    require: true
  },
  password : {
    type: String,
    require: true
  }
});

const Users = module.exports = mongoose.model('users',UserSchema);

module.exports.getAllUser =(callback)=> {
  Users.find({}, callback);
};

// module.exports.getUserById = (id, callback) => {
//   User.findById(id, callback);
// }

// module.exports.getUserByUsername = (username, callback) => {
//   const query = {username:username};
//   User.findOne(query, callback);
// }
// //https://www.npmjs.com/package/bcryptjs
// module.exports.addUser = (newUser, callback) => {
//   bcrypt.genSalt(10, (err, salt) => {
//     bcrypt.hash(newUser.password, salt, (err, hash) => {
//         if(err) throw err;
//         // Store hash in your password DB. 
//         newUser.password = hash;
//         newUser.save(callback);
//     });
//   });
// }

// module.exports.comparePassword = (candiPassword, hash, callback) => {
//   bcrypt.compare(candiPassword, hash, (err, isMatch) => {
//     if(err) throw err;
//     callback(null, isMatch);
//   });
// }
