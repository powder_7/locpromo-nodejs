const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Users = require('../models/users');
const Promos = require('../models/promos');
const {
  check,
  validationResult
} = require('express-validator/check');

//index 
router.get('/', [
    check('longitude', 'Longitude is required.').isLength({min: 1}),
    check('latitude', 'Latitude is required.').isLength({min: 1}),
    check('radius', 'Radius is required.').isLength({min: 1}),
    check('offset', 'offset is required.').isLength({min: 1})
    // check('limit', 'limit is required.').isLength({min: 1})
  ],
  (req, res, next) => {

    const errorFormatter = ({ location, msg, param }) => {
      // Build your resulting errors however you want! String, object, whatever - it works!
      return {msg};
    };
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
      console.log(errors.mapped());
      res.json( {
          success: false,
          errors: errors.mapped()
      });
    }
    else {
      let longitude = req.query.longitude;
      let latitude = req.query.latitude;
      let miles = req.query.radius;
      let offset = req.query.offset;
      let limit = req.query.limit;
      let distance = 1609.34 * miles;

      Promos.getList(longitude, latitude, distance, offset, limit, (err, result) => {
        if (err) {
          res.json({
            success: false,
            errors: err
          });
        } 
        else {
          res.json({
            success: true,
            promos: result
          });
        }
      });

    }
  });
  

//post to store data
router.post('/',[
    check('longitude', 'Longitude is required.').isLength({min: 1}),
    check('latitude', 'Latitude is required.').isLength({min: 1})
  ],
  (req, res) => {

    const errorFormatter = ({ location, msg, param }) => {
      // Build your resulting errors however you want! String, object, whatever - it works!
      return {msg};
    };
    const errors = validationResult(req).formatWith(errorFormatter);
    if (!errors.isEmpty()) {
      console.log(errors.mapped());
      res.json( {
          success: false,
          errors: errors.mapped()
      });
    }
    else {
      let input = {
        _id: new mongoose.Types.ObjectId(),
        user_id: 3,
        business_id: 3,
        website: "website.url",
        image: req.body.image,
        title: req.body.title,
        description: req.body.description,
        category: "Ads",
        price: req.body.price,
        street: req.body.street,
        city: req.body.city,
        zipcode: req.body.zipcode,
        location: { type: "Point", coordinates: [ req.body.longitude, req.body.latitude ] },
        temp: "something extra",
        expires_at: req.body.expires_at
      };
 
      let promos = new Promos(input);
      promos.save(err=>{
        if(err) { 
          console.log(err);
          res.json( {
            success: false,
            errors: err
          });
        }
        else {
          console.log('save successful');
          res.json( {
            success: true,
            promos: promos
          });
        }
      });
    }

});

//index 
router.delete('/:id',  (req, res, next) => {

  console.log(req.params);
  // console.log(req.body);
  // console.log(req.query);
  
  Promos.doDelete(req.params.id, (err, result) => {
    if (err) {
      res.json({
        success: false,
        errors: err
      });
    } 
    else {
      // console.log(JSON.stringify(req.body.id, null, 4));
      res.json({
        id: req.params.id,
        success: (result.n > 0)?true:false,
        promos: result
      });
    }
  });
   
});

//register 
router.get('/register', (req, res, next) => {
  // console.log('debug');
  res.send('Register');
});

list = (req) => {
  let longitude = url_query('longitude', req.url);
  let auth = req.get('Authorization');
  // console.log(longitude);
  console.log(auth);
}

// Parse URL Queries
function url_query(query, url) {
  query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var expr = "[\\?&]" + query + "=([^&#]*)";
  var regex = new RegExp(expr);
  var results = regex.exec(url);
  if (results !== null) {
    return results[1];
  } else {
    return false;
  }
}


module.exports = router;