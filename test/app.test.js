// let assert = require('assert');
// let request = require("request");
let server = require('../app.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
const { assert, expect} = require('chai');
chai.use(chaiHttp);
 
let url = "http://localhost:8080/promos?longitude=-122.08350000000002&latitude=37.390309&radius=5&limit=10&offset=0&token=EfOo-KTMUOYolo6rRM3wtg";

// let request = require("request");
// var expects = require('chai').expect;
// it("returns status 200 and json success is true", function() {
//     request(url, function(error, response, body) {
//         expects(response.statusCode).to.equal(200);
//         expects(response.body.succes).to.equal(true);
//     });
// });

describe('Basic Mocha String Test', function () {
    
    // it('should return number of charachters in a string', function () {
    //         assert.equal("Hello".length, 4);
    // });

    it('should return first charachter of the string', function () {
            assert.equal("Hello".charAt(0), 'H');
    });

    // before(function () {
    //     server.listen(8080);
    // });
   
    it('it should GET data and return 200 ', (done) => {
        chai.request(url)
            .get('/')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('Object');
                  expect(res.body.success).to.be.true;
                  assert.isTrue(res.body.success);
                //   console.log(res.body.promos);
              done();
            });
    });
 
});
 
describe('/POST ', () => {
    it('it POST data and return 200', (done) => {
        let input = {
            user_id: 99,
            business_id: 1,
            latitude : 37.390309,
            longitude :  -122.08350000000002,
            website: "website.url",
            image: "some.url",
            title: "$12.50 for $25 Worth of Pizza",
            description: "More than a dozen gourmet pies, with toppings such as shrimp, sun-dried tomatoes, and feta, complement an array of salads and sandwiches",
            price: "12.50",
            category: "Food & Drink",
            street: "123 A St",
            city: "Sacramento",
            zipcode: "91234",
            location: { type: "Point", coordinates: [ -122.08350000000002, 37.390309 ] },
            temp: "something extra",
            expires_at: "2018-01-01 23:59:59"
            };
        chai.request(server)
          .post('/promos')
          .send(input)
          .end((err, res) => {
                res.should.have.status(200);
                console.log(res.body.promos);
            done();
          });
    });
});

describe('/Delete ', () => {
    it('it DELETE data and return 200', (done) => {
        let id =  '5bc5593f7d6bae2f0baa920d';
        chai.request(server)
        .delete('/promos/'+id)
        // .send({id})
        .end((err, res) => {
              res.should.have.status(200);
              console.log('unit test DEL ');
              console.log(res.body.promos);
          done();
        });
    });

});